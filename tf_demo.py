#!/usr/bin/env python3

from scipy.constants import pi
import numpy as np
from matplotlib import pyplot as plt
from scipy.fftpack import fft, fftfreq
from scipy.signal import hann, spectrogram

def main():
    
    fs = 30
    Tmax = 1000
    Ts = 1.0/fs
    t = np.arange(0, Tmax, Ts)
  
    s_m = t / Tmax * 2

    fc = 10
    s_c = np.sin(2*pi*(fc+s_m)*t)

    plt.figure()
    plt.subplot(3,1,1)
    plt.plot(t,s_c)

    f = fftfreq(len(t), Ts)
    w = hann(len(t))
    plt.subplot(3,1,2)
    plt.stem(f, np.abs(fft(s_c*w)))

    f, t_spectrogram, Sxx = spectrogram(s_c, fs, window='hann')
    plt.subplot(3,1,3)
    plt.pcolormesh(t_spectrogram, f, Sxx)
    plt.show()    

if __name__=='__main__':
    main()
