#!/usr/bin/env python3

from scipy.signal import hann
from scipy.constants import pi
from scipy.fftpack import fft, fftfreq
import numpy as np
import matplotlib.pyplot as plt

def main():
    fs = 100
    Ts = 1.0/fs
    Tmax = 2.0

    t = np.arange(0, Tmax, Ts)

    frequencies = [5, 3.1]
    signals = []

    for frequency in frequencies:
        signals.append(np.sin(2*pi*frequency*t+0))

    w = hann(len(t))

    windowed_signals = []
    for signal in signals:
        windowed_signals.append(signal * w)

    colors = [ 'b', 'g' ]
    signal_sets = [ signals, windowed_signals ]
    titles = [ 'Original signals', 'Hann window' ]

    f = fftfreq(len(t), Ts)

    plt.figure(figsize=[12,9])

    for k, signal_set, title in zip(range(0,len(signal_sets),1), signal_sets, titles):

        plt.subplot(len(signal_sets), 2, 2*k+1)
        for frequency, signal, color in zip(frequencies, signal_set, colors):
            plt.stem(t, signal, label='%s Hz' % frequency, basefmt='k-', linefmt='%s-' % color, markerfmt='%so' % color, alpha=0.5)
        plt.legend()
        plt.title('%s (TD)' % title)
        plt.ylabel('Signal value')
        plt.xlabel('Time (s)')
 

        plt.subplot(len(signal_sets), 2, 2*k+2)
        for frequency, signal, color in zip(frequencies, signal_set, colors):
            plt.stem(f, np.abs(fft(signal)), label='%s Hz' % frequency, basefmt='k-', linefmt='%s-' % color, markerfmt='%so' % color, alpha=0.5)
        plt.legend()
        plt.title('%s (FFT)' % title)
        plt.ylabel('FFT value')
        plt.xlabel('Frequency (Hz)')

    plt.tight_layout()
    plt.show()

if __name__=='__main__':
    main()
