import numpy as np
from scipy.constants import pi
from scipy.fftpack import fft, fftfreq, fftshift
import matplotlib.pyplot as plt
from scipy.signal import hann

Ts = 0.1
t = np.arange(0,600,Ts)

x = 1 * np.sin(2*pi*2*t)
y = 1 * np.sin(2*pi*1.73*t)

F = fftfreq(t.size, d=Ts)

X = fft(x)/t.size
Y = fft(y)/t.size

w = hann(t.size)

plt.figure()
plt.plot(fftshift(F), fftshift(np.abs(X)))
plt.plot(fftshift(F), fftshift(np.abs(Y)))
plt.plot(fftshift(F), fftshift(np.abs(w*fft(y)/t.size)))
plt.show()

